# businessBudgetManagement

L'historique git permet de voir les évolutions des diagrammes
Les diagrammes sont complets et détaillés
<br/>
## useCase

Nous avons créer cinq acteurs liés par des généralisations:
- Employee
- Head of Department
- Buying Department
- Accounting Department
- General Management

Ces acteurs représentent des rôles applicables à l'utilisateur, leur donnant accès à toutes ses méthodes de base ainsi qu'à des méthodes supplémentaires, en fonction de leur rôle.

Le sujet principal est l'application budget, elle se découpe en trois sous sujets:
- Order
- Orders validation
- Orders history

Cette structure de sujet nous permet de découper les fonctionnalités et de les prioriser en fonction des besoins clients. 
<br/>
## Diagramme de classe

### Choix de conception
___
Tous les repository auront une interface dédiée permettant l'implémentation de méthodes similaires peu importe la technologie utilisée fonctionnellement.
Ce système applique donc par défaut l'inversion de dépendance selon le principe SOLID.

- order
- operation
- product
- employee
- category
- department
<br/>

#### **Employés**
___
Tous les employés auront un département assigné ainsi que des rôles, en fonction de leurs rôles ils auront accès à des fonctionnalitées différentes.

On déclare une fonction findByEmail dans l'interface repository des employés pour faciliter la connexion, qui se fera elle-même dans un controller via les méthodes de login et logout.
<br/>
#### **Departement**
___
Les départements étant liés aux utilisateurs et utilisés principalement au niveau des orders il n'était pas utile de leur créer un controller.

L'interface repository leur étant liée contiendra uniquement le crud.
<br/>
#### **Order**
___
Les orders représentent les commandes utilisateurs, cette entité contiendra toutes les données nécessaires à la gestion de ces commandes ainsi qu'une relation aux produits, eux-mêmes liés à leur catégorie.

Un lien a éé créé entre Order et Operation, ainsi qu'entre leurs controller/repository, afin de créer une opération à chaque commande passée par l'utilisateur, permettant ainsi aux supérieurs et/ou comptables de visualiser les opérations comptables et de garder une trace des paiements.

Un lien entre les orders et les départements a aussi été créé afin de filtrer les commandes par département, par exemple pour que les chefs d'un secteur spécifique ne voient que les commandes de leurs employés.

Thomas, Kevin et Fanny

